package project.ivan.airtouch_exercise.http;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by adrian on 1/19/2018.
 */

public class HttpResponseRates {
    Map<String, BigDecimal> mapRates;
    Map<String, String> mapFromTo;

    public HttpResponseRates() {
    }

    public HttpResponseRates(Map<String, BigDecimal> mapRates, Map<String, String> mapFromTo) {
        this.mapRates = mapRates;
        this.mapFromTo = mapFromTo;
    }

    public Map<String, BigDecimal> getMapRates() {
        return mapRates;
    }

    public void setMapRates(Map<String, BigDecimal> mapRates) {
        this.mapRates = mapRates;
    }

    public Map<String, String> getMapFromTo() {
        return mapFromTo;
    }

    public void setMapFromTo(Map<String, String> mapFromTo) {
        this.mapFromTo = mapFromTo;
    }
}
