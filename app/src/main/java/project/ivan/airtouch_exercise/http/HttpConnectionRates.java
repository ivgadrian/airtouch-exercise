package project.ivan.airtouch_exercise.http;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by adrian on 1/19/2018.
 */

public class HttpConnectionRates extends AsyncTask<String, Void, HttpResponseRates> {

    URL url;
    HttpURLConnection connection;


    @Override
    protected HttpResponseRates doInBackground(String... params) {

        try {
            url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();

            InputStream inputStream = connection.getInputStream();
            InputStreamReader inputStreamReader =
                    new InputStreamReader(inputStream);
            BufferedReader reader =
                    new BufferedReader(inputStreamReader);

            String line = reader.readLine();

            reader.close();
            inputStreamReader.close();
            inputStream.close();
            connection.disconnect();

            return getHttpResponseFromJson(line);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private HttpResponseRates getHttpResponseFromJson(String json)
            throws JSONException {

        if (json == null) {
            return null;
        }

        JSONArray jsonArray = new JSONArray(json);

        Map<String, BigDecimal> mapRates= new HashMap<String, BigDecimal>();
        Map<String, String> mapFromTo=new HashMap<String, String>();

        for(int i=0;i<jsonArray.length();i++){
            JSONObject item = jsonArray.getJSONObject(i);
            String from = item.getString("from");
            String to=item.getString("to");
            String keyForRates = from+to;
            Currency currency = Currency.getInstance(to);
            BigDecimal rate = new BigDecimal((item.getDouble("rate")));
            mapRates.put(keyForRates,
                    rate.setScale(currency.getDefaultFractionDigits(),BigDecimal.ROUND_HALF_EVEN));
        }

        mapFromTo.put("USD","AUD");
        mapFromTo.put("AUD","EUR");
        mapFromTo.put("CAD","USD");

        return new HttpResponseRates(mapRates, mapFromTo);
    }
}
