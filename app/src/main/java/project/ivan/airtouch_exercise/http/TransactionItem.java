package project.ivan.airtouch_exercise.http;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by adrian on 1/19/2018.
 */

public class TransactionItem {
    String sku;
    BigDecimal amount;
    Currency currency;

    public TransactionItem() {
    }

    public TransactionItem(String sku, BigDecimal amount, Currency currency) {
        this.sku = sku;
        this.amount = amount;
        this.currency = currency;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}

