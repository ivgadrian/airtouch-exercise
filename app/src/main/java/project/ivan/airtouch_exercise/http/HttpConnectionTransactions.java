package project.ivan.airtouch_exercise.http;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by adrian on 1/19/2018.
 */

public class HttpConnectionTransactions extends AsyncTask<String, Void, HttpResponseTransactions> {

    URL url;
    HttpURLConnection connection;


    @Override
    protected HttpResponseTransactions doInBackground(String... params) {

        try {
            url = new URL(params[0]);
            connection =
                    (HttpURLConnection) url.openConnection();

            InputStream inputStream = connection.getInputStream();
            InputStreamReader inputStreamReader =
                    new InputStreamReader(inputStream);
            BufferedReader reader =
                    new BufferedReader(inputStreamReader);

            String line = reader.readLine();

            reader.close();
            inputStreamReader.close();
            inputStream.close();
            connection.disconnect();

            return getHttpResponseFromJson(line);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private HttpResponseTransactions getHttpResponseFromJson(String json)
            throws JSONException {

        if (json == null) {
            return null;
        }

        JSONArray jsonArray = new JSONArray(json);

        List<TransactionItem> transactions = new ArrayList<TransactionItem>();

        for(int i=0;i<jsonArray.length();i++){
            JSONObject item = jsonArray.getJSONObject(i);

            transactions.add(new TransactionItem(
                    item.getString("sku"),
                    new BigDecimal(item.getDouble("amount")),
                    Currency.getInstance(item.getString("currency"))
            ));
        }

        return new HttpResponseTransactions(transactions);
    }
}