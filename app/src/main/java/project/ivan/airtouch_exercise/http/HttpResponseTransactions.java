package project.ivan.airtouch_exercise.http;

import java.util.List;

/**
 * Created by adrian on 1/19/2018.
 */

public class HttpResponseTransactions {
    List<TransactionItem> transactions;

    public HttpResponseTransactions(List<TransactionItem> transactions) {
        this.transactions = transactions;
    }

    public HttpResponseTransactions() {
    }

    public List<TransactionItem> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionItem> transactions) {
        this.transactions = transactions;
    }
}
