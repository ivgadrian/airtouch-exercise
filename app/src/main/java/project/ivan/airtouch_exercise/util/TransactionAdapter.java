package project.ivan.airtouch_exercise.util;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.List;

import project.ivan.airtouch_exercise.R;
import project.ivan.airtouch_exercise.http.TransactionItem;

/**
 * Created by adrian on 1/19/2018.
 */

public class TransactionAdapter extends ArrayAdapter{
    private int resource;
    private List<TransactionItem> objects;
    private LayoutInflater inflater;

    public TransactionAdapter(@NonNull Context context,
                           @LayoutRes int resource,
                           @NonNull List<TransactionItem> objects,
                           LayoutInflater inflater) {
        super(context, resource, objects);

        this.resource=resource;
        this.objects=objects;
        this.inflater=inflater;
    }

    @NonNull
    @Override
    public View getView(int position,
                        @Nullable View convertView,
                        @NonNull ViewGroup parent) {
        View row = inflater.inflate(resource,parent,false);

        TextView tvProduct = (TextView)row.findViewById(R.id.row_transaction_product);
        TextView tvAmount = (TextView)row.findViewById(R.id.row_transaction_amount);
        TextView tvCurrency = (TextView)row.findViewById(R.id.row_transaction_currency);

        TransactionItem transaction = objects.get(position);

        tvProduct.setText(transaction!=null && transaction.getSku()!=null ?  transaction.getSku():"");
        tvAmount.setText(transaction!=null && transaction.getAmount()!=null ?  transaction.getAmount().setScale(transaction.getCurrency().getDefaultFractionDigits(), BigDecimal.ROUND_HALF_EVEN).toString():"");
        tvCurrency.setText(transaction!=null && transaction.getCurrency()!=null ?  transaction.getCurrency().getCurrencyCode():"");

        return row;
    }
}
