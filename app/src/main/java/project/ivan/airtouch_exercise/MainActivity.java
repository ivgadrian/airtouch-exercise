package project.ivan.airtouch_exercise;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Map;

import project.ivan.airtouch_exercise.http.HttpConnectionRates;
import project.ivan.airtouch_exercise.http.HttpConnectionTransactions;
import project.ivan.airtouch_exercise.http.HttpResponseRates;
import project.ivan.airtouch_exercise.http.HttpResponseTransactions;
import project.ivan.airtouch_exercise.http.TransactionItem;
import project.ivan.airtouch_exercise.util.TransactionAdapter;

public class MainActivity extends AppCompatActivity {

    private static String URL_RATES="http://gnb.dev.airtouchmedia.com/rates.json";
    private static String URL_TRANSACTIONS="http://gnb.dev.airtouchmedia.com/transactions.json";
    TextView tvTotalAmount;
    Spinner spnProduct;
    ListView lvTransactions;
    List<TransactionItem> transactions;
    List<TransactionItem> filteredTransactions;
    List<String> spinnerValues;
    Map<String, BigDecimal> mapForRates;
    Map<String, String> mapFromTo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        httpConnection();
    }

    private BigDecimal convertToEur(TransactionItem t){
        TransactionItem transaction = new TransactionItem(t.getSku(),t.getAmount(),t.getCurrency());
        while(!transaction.getCurrency().getCurrencyCode().equals("EUR")){
            String newCurrency = mapFromTo.get(transaction.getCurrency().getCurrencyCode());
            BigDecimal rate = mapForRates.get(transaction.getCurrency().getCurrencyCode()+newCurrency);
            BigDecimal newAmount = transaction.getAmount().multiply(rate);
            transaction.setAmount(newAmount.setScale(transaction.getCurrency().getDefaultFractionDigits(), BigDecimal.ROUND_HALF_EVEN));
            transaction.setCurrency(Currency.getInstance(newCurrency));
        }
        return transaction.getAmount();
    }

    private BigDecimal totalAmount(){
        List<TransactionItem> newFilteredList = new ArrayList<TransactionItem>();
        newFilteredList.addAll(filteredTransactions);
        BigDecimal totalAmount=new BigDecimal(0);
        for(TransactionItem t:newFilteredList) {
            totalAmount=totalAmount.add(convertToEur(t));
        }
        return totalAmount;
    }



    public List<TransactionItem> getFilteredTransactionsByProduct(String product){
        filteredTransactions=new ArrayList<TransactionItem>();
        for(TransactionItem t:transactions){
            if(t.getSku().equals(product)){
                filteredTransactions.add(t);
            }
        }
        return filteredTransactions;
    }

    private List<String> getSpinnerValues(){
        spinnerValues=new ArrayList<String>();
        for(TransactionItem t:transactions){
            spinnerValues.add(t.getSku());
        }
        return spinnerValues;
    }

    private void setUpAdapterForListView(List<TransactionItem> transactions){
        TransactionAdapter adapter=new TransactionAdapter(getApplicationContext(),
                R.layout.lv_transaction_row,
                transactions,
                getLayoutInflater());
        lvTransactions.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void initComponents(){
        tvTotalAmount= (TextView) findViewById(R.id.tv_total_amount);
        spnProduct = (Spinner)findViewById(R.id.spinner_product);
        lvTransactions = (ListView)findViewById(R.id.lv_transactions);
    }

    private void httpConnection(){
        HttpConnectionRates connectionForRates = new HttpConnectionRates(){
            @Override
            protected void onPostExecute(HttpResponseRates httpResponseRates) {
                super.onPostExecute(httpResponseRates);
                if(httpResponseRates!=null){
                    mapForRates=httpResponseRates.getMapRates();
                    mapFromTo=httpResponseRates.getMapFromTo();
                }
            }
        };
        connectionForRates.execute(URL_RATES);
        HttpConnectionTransactions connectionForTransactions = new HttpConnectionTransactions(){
            @Override
            protected void onPostExecute(HttpResponseTransactions httpResponseTransactions) {
                super.onPostExecute(httpResponseTransactions);
                if(httpResponseTransactions!=null){
                    transactions=httpResponseTransactions.getTransactions();
                }
                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.support_simple_spinner_dropdown_item,
                        getSpinnerValues());
                spnProduct.setAdapter(spinnerAdapter);

                spnProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        String selectedItem = adapterView.getItemAtPosition(i).toString();
                        List<TransactionItem> filteredTransactions = getFilteredTransactionsByProduct(selectedItem);
                        setUpAdapterForListView(filteredTransactions);
                        BigDecimal totalAmount = totalAmount();
                        Currency currency = Currency.getInstance("EUR");
                        tvTotalAmount.setText(totalAmount.setScale(currency.getDefaultFractionDigits(), BigDecimal.ROUND_HALF_EVEN).toString());

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        };
        connectionForTransactions.execute(URL_TRANSACTIONS);
    }
}
